import { Injectable, HttpException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { User } from '../users/users.interface';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';
import { UsersModel } from '../users/users.model';
import { RollbarLogger } from 'nestjs-rollbar';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel('Users')
    private usersModel: Model<UsersModel>,
    private jwtService: JwtService,
    private readonly rollbarLogger: RollbarLogger,
  ) {}

  async validateUser(email: string, password: string) {
    const userData = await this.usersModel.findOne({ email });

    if (!userData) {
      throw new HttpException(
        'User with that email does not exist. Please signup',
        400,
      );
    }

    const isPasswordMatching = await bcrypt.compare(
      password,
      userData.password.toString(),
    );

    if (!isPasswordMatching) {
      this.rollbarLogger.log('Email and password do not match');
      throw new HttpException('Email and password do not match', 400);
    } else {
      return userData;
    }
  }

  async login(user: any) {
    const payload = { email: user.email, id: user.id };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async signup(user: User) {
    const newUser = new this.usersModel({
      username: user.username,
      email: user.email,
      password: await bcrypt.hash(user.password, 10),
    });

    try {
      const user = await newUser.save();
      delete user.password;

      return user;
    } catch (err) {
      this.rollbarLogger.error(err, 'userSigup');
      throw new HttpException('user already exists', 404);
    }
  }
}
