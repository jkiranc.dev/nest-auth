import { ConfigModule, ConfigService } from '@nestjs/config';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { LoggerModule } from 'nestjs-rollbar';
import config from './config/keys';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRoot(config.mongoURI),
    AuthModule,
    UsersModule,
    LoggerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        accessToken: configService.get('ROLLBAR_TOKEN'),
        environment: configService.get('ENVIRONMENT'),
      }),
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
