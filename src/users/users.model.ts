import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Users {
  @Prop({ unique: true })
  username: String;

  @Prop({ unique: true })
  email: String;

  @Prop()
  password: String;
}

export type UsersModel = Users & Document;

export const UsersSchema = SchemaFactory.createForClass(Users);
